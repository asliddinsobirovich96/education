package uz.medical.controller;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.medical.entity.Client;
import uz.medical.payload.ReqClient;
import uz.medical.payload.ReqCourse;

import uz.medical.service.ClientService;

import java.util.List;


@Controller
@RequestMapping("/client")
public class ClientController {

    final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

   /* @PostMapping("/attachment")
    @ResponseBody
    public Integer saveAttachment(@RequestParam(name = "file") MultipartFile multipartFile) throws IOException {
     return    attachmentService.saveAttachment(multipartFile);
    }*/

   /* @GetMapping("/attachmentImg")
    public String  getImg( Model model){
        final List<Byte[]> allByContent = attachmentRepository.findAllByContent();
        model.addAttribute("img", allByContent);
        return "Images";
    }*/

   /* @GetMapping("/attachmentImg")
    @ResponseBody
    public List<Byte[]> getImg(){
        final List<Byte[]> allByContent = attachmentRepository.findAllByContent();
        return allByContent;
    }

    @GetMapping("/img")
    public String img(Model model){
     *//*   model.addAttribute("img", attachmentRepository.findAllByContent());*//*
        model.addAttribute("img", attachmentRepository.findAll());
        return "Images";
    }*/


    @GetMapping("/list")
    public String clientList(Model model) {

        model.addAttribute("client", clientService.clientAllList());
        return "clients";
    }

    @GetMapping("/allList")
    public List<Client> listClient(){
        return clientService.clientAllList();
    }

    @GetMapping("/info/{id}")
    public String clientInfo(@PathVariable Integer id, Model model) {
        model.addAttribute("client", clientService.clientInfo(id));
        return "clientInfo";
    }

    @GetMapping("/add")
    public String openAddClient() {
        return "addClient";
    }

    @PostMapping(value = "save", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String saveClient(ReqClient reqClient) {
        clientService.saveClient(reqClient);
        return "redirect:/client/list";
    }

    @GetMapping("/update/{id}")
    public String updateClient(@PathVariable Integer id, Model model) {
        model.addAttribute("uClient", clientService.updateCliet(id));
        return "/updateForms/updateClient";
    }

    @PostMapping("/edit")
    public String editClient(
            Integer id,
            String fullName,
            Integer age,
            String address,
            String birthday,
            String phoneNumber,
            String email,
            String cause,
            String diagnos,
            String recommended) {
        clientService.editedClient(id, fullName, age, address, birthday, phoneNumber, email, cause, diagnos, recommended);
        return "redirect:/client/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Integer id) {
        clientService.deleteClient(id);
        return "redirect:/client/list";
    }

    @PostMapping(value = "course", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String saveCourse(ReqCourse reqCourse) {
        clientService.saveCourse(reqCourse);
        return "redirect:/client/list";
    }
}
