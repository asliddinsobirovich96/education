package uz.medical.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import sun.util.resources.cldr.rof.CalendarData_rof_TZ;
import uz.medical.entity.Employee;
import uz.medical.entity.Enums.RoleName;
import uz.medical.payload.ReqEmployee;
import uz.medical.payload.ReqSignIn;
import uz.medical.repository.EmployeeRepository;
import uz.medical.repository.RoleRepository;

import java.util.Optional;

@Controller
@RequestMapping("/sign")
public class SignController {
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    AuthenticationManager authenticationManager;

    @GetMapping("/up")
    public String openRegister() {
        return "signUp";
    }

    @PostMapping("/add")
    public String addRegister(@ModelAttribute ReqEmployee reqEmployee, Model model) {
        model.addAttribute("user", reqEmployee);
        return "password";
    }

    @PostMapping(value = "/up", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String registerEmployee(ReqEmployee reqEmployee) {
        final Optional<Employee> byUsername = employeeRepository.findByUsername(reqEmployee.getUsername());

        if (byUsername.isPresent()) {
            return "home";
        } else {
           employeeRepository.save(new Employee(
                   reqEmployee.getFish(),
                   reqEmployee.getFan(),
                   reqEmployee.getTajribasi(),
                   reqEmployee.getMuvaffaqiyat(),
                   reqEmployee.getYoutobelink(),
                   reqEmployee.getTalabahududi(),
                   reqEmployee.getNarxi(),
                   reqEmployee.getVaqtlari(),
                   reqEmployee.getKursjoyi(),
                   reqEmployee.getTelefon(),
                   reqEmployee.getUsername(),
                   passwordEncoder.encode(reqEmployee.getPassword()),
                   roleRepository.findAllByRole(RoleName.ROLE_TEACHER)
            ));
            return "login";
        }
    }

    @GetMapping("/login")
    public String goSignInPage(@RequestParam(defaultValue = "false") boolean error, Model model) {
        model.addAttribute("error", error);
        return "login";
    }

    @PostMapping(value = "/in", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String signIn(ReqSignIn reqSignIn) {
        final Optional<Employee> byUsername = employeeRepository.findByUsername(reqSignIn.getUsername());
        if (byUsername.isPresent()) {
            final Employee employee = byUsername.get();
            if (passwordEncoder.matches(reqSignIn.getPassword(), employee.getPassword())) {
                final Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(reqSignIn.getUsername(), reqSignIn.getPassword()));
                SecurityContextHolder.getContext().setAuthentication(authenticate);

                return "home";
            }
        }
        return "redirect:/sign/login?error=true";
    }
}
