package uz.medical.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.medical.service.EmployeeService;


@Controller
@RequestMapping("/employee")
public class EmployeeController {
    final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }


    @GetMapping("/list")
    public String emplList(Model model) {
        model.addAttribute("newEmployee", employeeService.employeeAllList());
        return "employeeList";
    }

    @GetMapping("/cabinet")
    public String openCabinetPage(Model model) {
        model.addAttribute("user", employeeService.employeeAllList());
        return "cabinet";
    }

    @GetMapping("/update/{id}")
    public String updateEmpl(@PathVariable Integer id, Model model) {
        model.addAttribute("updateEmpl", employeeService.getUpdateEmpl(id));
        return "/updateForms/updateEmployee";
    }

    @PostMapping("/edit")
    public String editEmpl(
            Integer id,
            String fish,
            String fan,
            String tajribasi,
            String muvaffaqiyat,
            String youtobelink,
            String talabahududi,
            double narxi,
            String vaqtlari,
            String kursjoyi,
            String telefon,
            String username,
            String password) {
        employeeService.editedEmpl(id, fish, fan, tajribasi, muvaffaqiyat, youtobelink, talabahududi, narxi, vaqtlari, kursjoyi, telefon, username, password);
        return "redirect:/employee/list";
    }



    @GetMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable Integer id) {
        employeeService.delete(id);
        return "redirect:/employee/list";
    }
}
