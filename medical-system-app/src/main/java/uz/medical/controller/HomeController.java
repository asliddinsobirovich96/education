package uz.medical.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.medical.service.EmployeeService;

@Controller
@RequestMapping("/home")
public class HomeController {
    final EmployeeService employeeService;

    public HomeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public String openHomePage() {
        return "home";
    }



}
