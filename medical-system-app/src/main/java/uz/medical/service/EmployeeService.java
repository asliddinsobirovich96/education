package uz.medical.service;

import org.springframework.stereotype.Service;
import uz.medical.entity.Employee;
import uz.medical.entity.Role;
import uz.medical.repository.EmployeeRepository;
import uz.medical.repository.RoleRepository;

import java.util.List;
import java.util.Optional;


@Service
public class EmployeeService {
    final RoleRepository roleRepository;

    final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository, RoleRepository roleRepository) {
        this.employeeRepository = employeeRepository;
        this.roleRepository = roleRepository;
    }

    public List<Employee> employeeAllList() {
        return employeeRepository.findAll();
    }

    public void delete(Integer id) {
        final Optional<Employee> employee = employeeRepository.findById(id);
        if (employee.isPresent()) {
            final Employee employee1 = employee.get();
            employeeRepository.delete(employee1);
        }
    }

    public Employee getUpdateEmpl(Integer id) {
        return employeeRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Not found employee updated id " + id));
    }



    public void editedEmpl(
            Integer id,
            String fish,
            String fan,
            String tajribasi,
            String muvaffaqiyat,
            String youtobelink,
            String talabahududi,
            double narxi,
            String vaqtlari,
            String kursjoyi,
            String telefon,
            String username,
            String password)
    {
        final Employee employee = employeeRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Not found employee updated id " + id));
        final List<Role> roles = employee.getRoles();
        employeeRepository.save(new Employee(
                id,
                fish,
                fan,
                tajribasi,
                muvaffaqiyat,
                youtobelink,
                talabahududi,
                narxi,
                vaqtlari,
                kursjoyi,
                telefon,
                username,
                password,
                roles
        ));
    }

}
