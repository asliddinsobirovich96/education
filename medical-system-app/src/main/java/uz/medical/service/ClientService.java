package uz.medical.service;

import org.springframework.stereotype.Service;
import uz.medical.entity.Client;
import uz.medical.entity.Course;
import uz.medical.entity.Enums.RoleName;
import uz.medical.payload.ReqClient;
import uz.medical.payload.ReqCourse;
import uz.medical.repository.ClientRepository;
import uz.medical.repository.CourseRepository;
import uz.medical.repository.RoleRepository;

import java.util.Collections;
import java.util.List;

@Service
public class ClientService {
    final ClientRepository clientRepository;
    final CourseRepository courseRepository;
    final RoleRepository roleRepository;

    public ClientService(ClientRepository clientRepository, CourseRepository courseRepository, RoleRepository roleRepository) {
        this.clientRepository = clientRepository;
        this.courseRepository = courseRepository;
        this.roleRepository = roleRepository;
    }

    public List<Client> clientAllList() {
        return clientRepository.findAll();
    }

    public Client saveClient(ReqClient reqClient) {


        final Course saveCourse = courseRepository.save(new Course(
                reqClient.getFullName(),
                reqClient.getAge(),
                reqClient.getAddress(),
                reqClient.getBirthday(),
                reqClient.getCause(),
                reqClient.getDiagnos(),
                reqClient.getRecommended()
        ));

        return clientRepository.save(new Client(
                reqClient.getFullName(),
                reqClient.getAge(),
                reqClient.getAddress(),
                reqClient.getBirthday(),
                reqClient.getPhoneNumber(),
                reqClient.getEmail(),
                reqClient.getCause(),
                reqClient.getDiagnos(),
                reqClient.getRecommended(),
                Collections.singletonList(saveCourse),
                roleRepository.findAllByRole(RoleName.ROLE_CLIENT)
        ));
    }

    public void deleteClient(Integer id) {
        clientRepository.deleteById(id);
        courseRepository.deleteById(id);
    }

    public Client updateCliet(Integer id) {
        return clientRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Not found client id " + id));
    }

    public void editedClient(
            Integer id,
            String fullName,
            Integer age,
            String address,
            String birthday,
            String phoneNumber,
            String email,
            String cause,
            String diagnos,
            String recommended) {
        final Course course = courseRepository.save(new Course(id, fullName, age, address, birthday, cause, diagnos, recommended));

        clientRepository.save(new Client(
                id,
                fullName,
                age, address,
                birthday,
                phoneNumber,
                email, cause,
                diagnos,
                recommended,
                Collections.singletonList(course),
                roleRepository.findAllByRole(RoleName.ROLE_CLIENT)

        ));
    }

    public void saveCourse(ReqCourse reqCourse) {
     courseRepository.save(new Course(
                reqCourse.getFullName(),
                reqCourse.getAge(),
                reqCourse.getAddress(),
                reqCourse.getBirthday(),
                reqCourse.getCause(),
                reqCourse.getDiagnos(),
                reqCourse.getRecommended()
        ));
    }

    public Client clientInfo(Integer  id){
        return clientRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Client id not found! id = " + id));
    }


}
