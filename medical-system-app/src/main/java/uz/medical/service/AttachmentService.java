/*
package uz.medical.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.medical.entity.Attachment;
import uz.medical.repository.AttachmentRepository;

import java.io.IOException;

@Service
public class AttachmentService {
    @Autowired
    AttachmentRepository attachmentRepository;

    public Integer saveAttachment(MultipartFile multipartFile) {
        try {
            Attachment attachment = new Attachment();
            attachment.setName(multipartFile.getOriginalFilename());
            attachment.setContent(multipartFile.getBytes());
            attachment.setContentType(multipartFile.getContentType());
            attachment.setSize(multipartFile.getSize());
            final Attachment attachment1 = attachmentRepository.save(attachment);
            return attachment1.getId();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
*/
