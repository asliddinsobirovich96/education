package uz.medical.payload;

import lombok.Data;


@Data
public class ReqClient {
    private String fullName;
    private Integer age;
    private String address;
    private String birthday;
    private String phoneNumber;
    private String email;
    private String cause;
    private String diagnos;
    private String recommended;
}
