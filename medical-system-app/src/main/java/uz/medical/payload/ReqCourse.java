package uz.medical.payload;

import lombok.Data;


@Data
public class ReqCourse {
    private Integer id;
    private String fullName;
    private Integer age;
    private String address;
    private String birthday;
    private String cause;
    private String diagnos;
    private String recommended;


}
