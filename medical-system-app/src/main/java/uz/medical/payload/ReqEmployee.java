package uz.medical.payload;

import lombok.Data;

import javax.persistence.Column;

@Data
public class ReqEmployee {
    private String fish;
    private String fan;
    private String tajribasi;
    private String muvaffaqiyat;
    private String youtobelink;
    private String talabahududi;
    private double narxi;
    private String vaqtlari;
    private String kursjoyi;
    private String telefon;

    private String username;
    private String password;
}
