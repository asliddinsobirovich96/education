package uz.medical.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String fullName;
    private Integer age;
    private String address;
    private String birthday;
    private String cause;
    private String diagnos;
    private String recommended;


    @CreationTimestamp
    @Column(updatable = false)
    private Timestamp createdAt;

    @UpdateTimestamp
    private Timestamp updatedAt;

    public Course(String fullName, Integer age, String address, String birthday, String cause, String diagnos, String recommended) {
        this.fullName = fullName;
        this.age = age;
        this.address = address;
        this.birthday = birthday;
        this.cause = cause;
        this.diagnos = diagnos;
        this.recommended = recommended;
    }

    public Course(Integer id, String fullName, Integer age, String address, String birthday, String cause, String diagnos, String recommended) {
        this.id = id;
        this.fullName = fullName;
        this.age = age;
        this.address = address;
        this.birthday = birthday;
        this.cause = cause;
        this.diagnos = diagnos;
        this.recommended = recommended;
    }
}
