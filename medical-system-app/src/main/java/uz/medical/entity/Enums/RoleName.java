package uz.medical.entity.Enums;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_MANAGER,
    ROLE_DIRECTOR,
    ROLE_BUGALTER,
    ROLE_CLIENT,
    ROLE_TEACHER
}
