package uz.medical.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String fullName;
    private Integer age;
    private String address;
    private String birthday;
    private String phoneNumber;
    private String email;
    private String cause;
    private String diagnos;
    private String recommended;

    @OneToMany
    private List<Course> courses;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "client_role",
            joinColumns = {@JoinColumn(name = "client_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<Role> role;


    @CreationTimestamp
    @Column(updatable = false)
    private Timestamp createdAt;

    @UpdateTimestamp
    private Timestamp updatedAt;

    public Client(String fullName, Integer age, String address, String birthday,String phoneNumber, String email, String cause, String diagnos, String recommended, List<Course> courses, List<Role> role) {
        this.fullName = fullName;
        this.age = age;
        this.address = address;
        this.birthday = birthday;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.cause = cause;
        this.diagnos = diagnos;
        this.recommended = recommended;
        this.courses = courses;
        this.role = role;
    }

    public Client(Integer id, String fullName, Integer age, String address, String birthday, String phoneNumber, String email, String cause, String diagnos, String recommended, List<Course> courses, List<Role> role) {
        this.id = id;
        this.fullName = fullName;
        this.age = age;
        this.address = address;
        this.birthday = birthday;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.cause = cause;
        this.diagnos = diagnos;
        this.recommended = recommended;
        this.courses = courses;
        this.role = role;
    }
}
