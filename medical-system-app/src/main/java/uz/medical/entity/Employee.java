package uz.medical.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Employee implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String fish;

    private String fan;

    private String tajribasi;
    private String muvaffaqiyat;
    private String youtobelink;
    private String talabahududi;
    private double narxi;
    private String vaqtlari;
    private String kursjoyi;
    private String telefon;

    @Column(unique = true)
    private String username;
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_role",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<Role> roles;

    @CreationTimestamp
    @Column(updatable = false)
    private Timestamp createdAt;

    @UpdateTimestamp
    private Timestamp updatedAt;

    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;

    public Employee(String fish, String fan, String tajribasi, String muvaffaqiyat, String youtobelink, String talabahududi, double narxi, String vaqtlari, String kursjoyi, String telefon, String username, String password, List<Role> roles) {
        this.fish = fish;
        this.fan = fan;
        this.tajribasi = tajribasi;
        this.muvaffaqiyat = muvaffaqiyat;
        this.youtobelink = youtobelink;
        this.talabahududi = talabahududi;
        this.narxi = narxi;
        this.vaqtlari = vaqtlari;
        this.kursjoyi = kursjoyi;
        this.telefon = telefon;
        this.username = username;
        this.password = password;
        this.roles = roles;
        this.accountNonExpired = true;
        this.accountNonLocked = true;
        this.credentialsNonExpired = true;
        this.enabled = true;
    }

    public Employee(Integer id, String fish, String fan, String tajribasi, String muvaffaqiyat, String youtobelink, String talabahududi, double narxi, String vaqtlari, String kursjoyi, String telefon, String username, String password, List<Role> roles) {
        this.id = id;
        this.fish = fish;
        this.fan = fan;
        this.tajribasi = tajribasi;
        this.muvaffaqiyat = muvaffaqiyat;
        this.youtobelink = youtobelink;
        this.talabahududi = talabahududi;
        this.narxi = narxi;
        this.vaqtlari = vaqtlari;
        this.kursjoyi = kursjoyi;
        this.telefon = telefon;
        this.username = username;
        this.password = password;
        this.roles = roles;
        this.accountNonExpired = true;
        this.accountNonLocked = true;
        this.credentialsNonExpired = true;
        this.enabled = true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roles;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
