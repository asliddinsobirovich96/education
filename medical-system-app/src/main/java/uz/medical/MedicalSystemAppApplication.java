package uz.medical;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class MedicalSystemAppApplication {

    public static void main(String[] args) {
       SpringApplication.run(MedicalSystemAppApplication.class, args);
    }

}
