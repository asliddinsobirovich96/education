package uz.medical.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.medical.entity.Role;
import uz.medical.entity.Enums.RoleName;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    List<Role> findAllByRole(RoleName roleName);
}
