package uz.medical.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.medical.entity.Employee;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface  EmployeeRepository extends JpaRepository<Employee, Integer> {
    Optional<Employee> findByUsername(String username);

    @Query(value = "select * from public.employee  order by created_at", nativeQuery = true)
    List<Employee> findAll();
}
