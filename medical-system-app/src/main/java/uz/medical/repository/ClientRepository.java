package uz.medical.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.medical.entity.Client;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Integer> {

    @Query(value = "select * from public.client order by created_at", nativeQuery = true)
    List<Client> findAll();


 }
