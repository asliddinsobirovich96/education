package uz.medical.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.medical.entity.Course;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course, Integer> {

    List<Course> findAllById(Integer id);

}
