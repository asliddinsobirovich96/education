/*
package uz.medical.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.w3c.dom.stylesheets.LinkStyle;
import uz.medical.entity.Attachment;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {

   @Query(value = "select max(id) from public.attachment", nativeQuery = true)
    Optional<Attachment> findById();

   @Query(value = "select content from public.attachment", nativeQuery = true)
   List<Byte[]> findAllByContent();
}
*/
