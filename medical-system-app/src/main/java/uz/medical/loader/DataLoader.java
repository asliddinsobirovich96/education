package uz.medical.loader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.medical.entity.Employee;
import uz.medical.entity.Enums.RoleName;
import uz.medical.repository.EmployeeRepository;
import uz.medical.repository.RoleRepository;

@Component
public class DataLoader implements CommandLineRunner {
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RoleRepository roleRepository;
    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;

    @Override
    public void run(String... args) throws Exception {

        if (initialMode.equals("always")) {
            employeeRepository.save(new Employee(
            "Asliddin Tursunov",
                    "Web dasturlash",
                    "4 yil",
                    "pdp sertificate",
                    "yo'q",
                    "Andijon",
                    12.3,
                    "har doim",
                    "Toshkent, yunusobod",
                    "+998 97 007 97 90",
                    "asliddin",
                    passwordEncoder.encode("12345"),
                    roleRepository.findAllByRole(RoleName.ROLE_ADMIN)
            ));

        }

    }
}
