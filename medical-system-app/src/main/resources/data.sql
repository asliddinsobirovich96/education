insert into role (id, role) values (1, 'ROLE_ADMIN');
insert into role (id, role) values (2, 'ROLE_MANAGER');
insert into role (id, role) values (3, 'ROLE_DIRECTOR');
insert into role (id, role) values (4, 'ROLE_BUGALTER');
insert into role (id, role) values (5, 'ROLE_CLIENT');
insert into role (id, role) values (6, 'ROLE_TEACHER');